//
//  main.swift
//  Learning Swift and Xcode
//
//  Created by Ethan Cha on 4/5/18.
//  Copyright © 2018 Ethan Cha. All rights reserved.
//
import Foundation


// If statements
print("Are you old enough?")

let minimum_age = 18
let current_age1 = Int(readLine()!)! // readLine similar to Python input(), remember to unwrap with !

if current_age1 < minimum_age {
    print("You are not old enough!")
} else {
    print("You are old enough!")
}

// Loops
print("Repeatedly doubling!")
sleep(2)

func double(start: Int, times: Int) -> Void {
    // Continuously doubling from *start* number for *times* times
    var n = start
    for _ in 1 ... times {
        print(n)
        n *= 2
    }
}

double(start: 1, times: 20)
